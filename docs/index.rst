Welcome to Rak'n'Rok documentation!
===================================

*Rak'n'Rok* is the umbrella project building and managing virtual datacenters.


.. toctree::
    :hidden:

    Krake <https://rak-n-rok.readthedocs.org/projects/krake/>


Krake_
    Krake [ˈkʀaːkə] is an orchestrator engine for containerized and virtualized
    workloads accross distributed and heterogeneous cloud platforms. It creates a
    thin layer of aggregaton on top of the different platforms (such as OpenStack,
    Kubernetes or OpenShift) and presents them through a single interface to the
    cloud user.

    The project is developed on `GitLab.com`_.


.. _Krake: /projects/krake/
.. _GitLab.com: https://gitlab.com/rak-n-rok/krake/
