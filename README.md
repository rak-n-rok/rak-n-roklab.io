# rak-n-rok.gitlab.io

This repository contains the source for the website of the Rak'n'Rok project.

Right now, only the [Read the Docs](http://rak-n-rok.rtfd.io/) landing page is
built powered by this repository.

Eventually, Rak'n'Rok will have a web presence using
[GitLab Pages](https://about.gitlab.com/product/pages/).
